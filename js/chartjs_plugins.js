/**
 * @file
 * Contains js for chartjs plugins.
 */

/* eslint-disable no-unused-vars */

var halfdonutTotal = {
  beforeDraw: function (chart) {

    'use strict';

    // Define vars.
    const width = chart.width;
    let height = chart.$context.chart.height;
    const ctx = chart.$context.chart.ctx;

    // Calculate position of text.
    if (chart.legend.options.display === true) {
      if (chart.legend.position === 'bottom') {
        height = height - chart.legend.height;
      }
    }
    ctx.restore();

    // Calculate font size.
    const fontSize = (height / 120).toFixed(2);
    ctx.font = fontSize + 'em sans-serif';

    // Set aligns.
    ctx.textBaseline = 'bottom';
    ctx.textAlign = 'center';

    // Create text.
    const text = chart.options.title.text;
    const textX = Math.round(width / 2);
    ctx.fillText(text, textX, height);
    ctx.save();
  }
};

/* eslint-enable no-unused-vars */
